from django import forms
from django.contrib.auth.forms import UserCreationForm

from .models import User


class CustomUserCreationForm(UserCreationForm):
    username = forms.CharField(label="Логин")
    first_name = forms.CharField(max_length=30, required=True, label="Ваше имя", help_text='Обязательное поле.')
    last_name = forms.CharField(max_length=30, required=True, label="Фамилия", help_text='Обязательное поле.')
    birth_date = forms.DateField(label="Дата рождения", help_text='Формат: ГГГГ-ММ-ДД')

    class Meta:
        model = User
        fields = UserCreationForm.Meta.fields + ('first_name', 'last_name', 'birth_date')
