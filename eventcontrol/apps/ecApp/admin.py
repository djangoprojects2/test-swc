from django.contrib import admin
from .models import Event, User


admin.site.register(User)
admin.site.register(Event)
