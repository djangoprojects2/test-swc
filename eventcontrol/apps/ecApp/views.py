from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as auth_login
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views import View
from rest_framework.generics import get_object_or_404

from .events import get_event_details, join_leave_event
from .forms import CustomUserCreationForm
from .models import User, Event


class MainPageView(View):
    """
    Главная страница приложения.
    Отображает список всех событий и событий, в которых участвует пользователь (если он аутентифицирован).
    """

    def get(self, request):
        if request.user.is_authenticated:
            all_events = Event.objects.all()
            user_events = Event.objects.filter(participants=request.user)

            context = {
                'all_events': all_events,
                'user_events': user_events,
            }
            return render(request, 'ecApp/index.html', context)

        else:
            return render(request, 'ecApp/index.html')


class EventDetailView(View):
    """
    Подробная информация о событии.
    Отображает информацию о событии, его участниках и позволяет пользователю присоединиться или покинуть событие.
    """

    def get(self, request, event_id):
        data = get_event_details(event_id, request.user)
        return JsonResponse(data)


class JoinLeaveEventView(View):
    """
    Присоединение или покидание события.
    Обрабатывает POST-запросы для присоединения или отказа от участия в событии пользователем.
    """

    def post(self, request, event_id):
        join_event = request.POST.get('join_event') == 'true'
        data = join_leave_event(event_id, request.user, join_event)
        return JsonResponse(data)


class UserDetailView(View):
    """
    Подробная информация о пользователе.
    Отображает информацию о пользователе.
    """

    def get(self, request, user_id):
        user = get_object_or_404(User, id=user_id)

        user_data = {
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'birth_date': user.birth_date
        }
        return JsonResponse(user_data)


class SignupView(View):
    """
    Регистрация нового пользователя.
    Отображает форму регистрации и обрабатывает POST-запрос для создания нового пользователя.
    """

    def get(self, request):
        form = CustomUserCreationForm()
        return render(request, 'ecApp/signup.html', {'form': form})

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Регистрация"
        return context

    def post(self, request):
        form = CustomUserCreationForm(request.POST)

        if form.is_valid():
            user = form.save()
            auth_login(request, user)
            return redirect('home')

        return render(request, 'ecApp/signup.html', {'form': form})


class LoginView(View):
    """
    Авторизация пользователя.
    Отображает форму авторизации и обрабатывает POST-запрос для аутентификации пользователя.
    """

    def get(self, request):
        form = AuthenticationForm()
        return render(request, 'ecApp/login.html', {'form': form})

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Авторизация"
        return context

    def post(self, request):
        form = AuthenticationForm(request, data=request.POST)

        if form.is_valid():
            auth_login(request, form.get_user())
            return redirect('home')

        return render(request, 'ecApp/login.html', {'form': form})
