from django.contrib.auth.views import LogoutView
from django.urls import path

from .views import MainPageView, SignupView, LoginView, EventDetailView, JoinLeaveEventView, UserDetailView


urlpatterns = [
    path('home/', MainPageView.as_view(), name='home'),
    path('signup/', SignupView.as_view(), name='signup'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('event/<int:event_id>/', EventDetailView.as_view(), name='event_detail'),
    path('event/<int:event_id>/join_leave/', JoinLeaveEventView.as_view(), name='join_leave_event'),
    path('user/<int:user_id>/detail/', UserDetailView.as_view(), name='user_detail')
]


