from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    first_name = models.CharField(max_length=50, blank=False, verbose_name="Имя")
    last_name = models.CharField(max_length=50, blank=False, verbose_name="Фамилия")
    date_joined = models.DateTimeField(auto_now_add=True, verbose_name="Дата регистрации")
    birth_date = models.DateField(blank=True, null=True, verbose_name="Дата рождения")

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class Event(models.Model):
    title = models.CharField(max_length=255, blank=False, verbose_name="Заголовок")
    text = models.TextField(blank=False, verbose_name="Текст")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_events', verbose_name="Создатель")
    participants = models.ManyToManyField(User, related_name='events_participated', verbose_name="Участники")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = "События"
