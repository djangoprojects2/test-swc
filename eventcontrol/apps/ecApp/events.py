from django.shortcuts import get_object_or_404
from .models import Event

def get_event_details(event_id, user):
    event = get_object_or_404(Event, id=event_id)
    is_user_participant = event.participants.filter(id=user.id).exists()
    participants_data = [{'id': participant.id, 'username': participant.username} for participant in event.participants.all()]

    data = {
        'title': event.title,
        'text': event.text,
        'created_at': event.created_at.strftime('%Y-%m-%d %H:%M:%S'),
        'participants': participants_data,
        'is_user_participant': is_user_participant,
    }
    return data

def join_leave_event(event_id, user, join_event):
    event = get_object_or_404(Event, id=event_id)

    if join_event:
        event.participants.add(user)
    else:
        event.participants.remove(user)

    is_user_participant = event.participants.filter(id=user.id).exists()
    button_text = 'Отказаться от участия' if is_user_participant else 'Принять участие'

    data = {
        'is_user_participant': is_user_participant,
        'button_text': button_text,
    }
    return data
