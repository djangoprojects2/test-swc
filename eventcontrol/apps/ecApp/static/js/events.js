$(document).ready(function () {
    $('.event-link').on('click', function (e) {
        e.preventDefault();
        var event_id = $(this).data('event-id');
        var joinLeaveUrl = '/event/' + event_id + '/join_leave/';
        $.ajax({
            url: '/event/' + event_id,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $('.event-details').html('<h1>Информация о событии</h1>' +
                    '<p><strong>Название:</strong> ' + data.title + '</p>' +
                    '<p><strong>Описание:</strong> ' + data.text + '</p>' +
                    '<p><strong>Дата создания:</strong> ' + data.created_at + '</p>' +
                    '<p><strong>Участники:</strong></p>' +
                    '<ul>' + data.participants.map(function (participant) {
                        console.log(data.participants)
                        return '<li><a href="#" class="user-link" data-user-id="' + participant.id + '">' + participant.username + '</a></li>';
                    }).join('') + '</ul>' +
                    '<button type="submit" class="join-leave-button btn btn-secondary bg-light text-dark">' + (data.is_user_participant ? 'Отказаться от участия' : 'Принять участие') + '</button>'
                );

                // Обработчик клика для кнопки "Принять участие" или "Отказаться от участия"
                $('.join-leave-button').on('click', function () {
                    var joinEvent = !data.is_user_participant;
                    $.ajax({
                        url: joinLeaveUrl,
                        type: 'POST',
                        data: {
                            'join_event': joinEvent,
                            'csrfmiddlewaretoken': csrf_token  // Добавьте это для защиты от CSRF-атак
                        },
                        dataType: 'json',
                        success: function (response) {
                            // Обновите кнопку после успешного действия
                            data.is_user_participant = response.is_user_participant;
                            $('.join-leave-button').text(response.button_text);
                            location.reload();
                        },
                        error: function () {
                            alert('Произошла ошибка при выполнении действия.');
                        }
                    });
                });
                // Обработчик клика на ссылке участника
                $('.user-link').on('click', function (e) {
                    e.preventDefault();
                    var user_id = $(this).data('user-id');
                    var userDetailUrl = '/user/' + user_id + '/detail/';
                    $.ajax({
                        url: userDetailUrl,
                        type: 'GET',
                        dataType: 'json',
                        success: function (userData) {
                            // Отображаем информацию об участнике в div ".user-details"
                            $('.user-details').html('<h2>Информация об участнике</h2>' +
                                '<p><strong>Логин:</strong> ' + userData.username + '</p>' +
                                '<p><strong>Имя:</strong> ' + userData.first_name + '</p>' +
                                '<p><strong>Фамилия:</strong> ' + userData.last_name + '</p>' +
                                '<p><strong>Дата рождения:</strong> ' + userData.birth_date + '</p>');
                        },
                        error: function () {
                            alert('Произошла ошибка при загрузке информации об участнике.');
                        }
                    });
                });
            },
            error: function () {
                alert('Произошла ошибка при загрузке информации о событии.');
            }
        });
    });
});