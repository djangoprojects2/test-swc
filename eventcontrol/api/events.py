from django.shortcuts import get_object_or_404
from apps.ecApp.models import Event

def get_event_by_id(event_id):
    return get_object_or_404(Event, id=event_id)