from djoser.serializers import UserCreateSerializer
from rest_framework import serializers
from apps.ecApp.models import User, Event


class UserSerializer(UserCreateSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'birth_date')


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ('title', 'text', 'participants')
