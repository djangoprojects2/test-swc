from django.urls import path, include
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from .views import *


urlpatterns = [
    path('auth/', include('djoser.urls')),
    path('auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token-login/', TokenLoginView.as_view(), name='token-login'),
    path('events/create/', EventCreateView.as_view(), name='event-create'),
    path('events/', EventListView.as_view(), name='event-list'),
    path('events/participate/<int:event_id>/', EventParticipateView.as_view(), name='event-participate'),
    path('events/cancel-participation/<int:event_id>/', EventCancelParticipationView.as_view(), name='event-cancel-participation'),
    path('events/delete/<int:event_id>/', EventDeleteView.as_view(), name='event-delete'),
]


