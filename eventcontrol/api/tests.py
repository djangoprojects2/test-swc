from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from apps.ecApp.models import Event, User

class EventCreateViewTest(APITestCase):
    """
    Тест на создание события
    """

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword'
        )
        self.client.force_authenticate(user=self.user)

    def test_create_event(self):
        url = reverse('event-create')
        data = {
            'title': 'Test Event',
            'text': 'This is a test event',
            'participants': [1]
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class EventParticipateViewTest(APITestCase):
    """
    Тест на возможность присойдениться к событию
    """

    def setUp(self):
        self.user = User.objects.create_user(
            username='testuser',
            password='testpassword'
        )

        self.event = Event.objects.create(
            title='Test Event',
            text='This is a test event',
            creator=self.user
        )

        self.url = reverse('event-participate', args=[self.event.id])
        self.client.force_authenticate(user=self.user)

    def test_participate_in_event(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], 'Вы участвуете в событии.')

        self.assertTrue(self.user in self.event.participants.all())
