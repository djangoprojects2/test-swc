from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import TokenError, AccessToken
from rest_framework.generics import CreateAPIView, ListAPIView, get_object_or_404

from .events import get_event_by_id
from .serializers import EventSerializer
from apps.ecApp.models import User, Event


class TokenLoginView(APIView):
    """
    Вход с помощью токена.
    Это представление позволяет пользователям войти в систему, предоставив действующий токен доступа.
    """

    def post(self, request):
        access_token = request.data.get('access_token')

        if not access_token:
            return Response({'error': 'Access token is required.'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            decoded_token = AccessToken(token=access_token)
            user_id = decoded_token.payload['user_id']
            user = User.objects.get(id=user_id)

            return Response({
                "id": user.id,
                "username": user.username,
                "email": user.email,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "birth_date": user.birth_date,
            })
        except TokenError:
            return Response({'error': 'Invalid access token.'}, status=status.HTTP_401_UNAUTHORIZED)


class EventCreateView(CreateAPIView):
    """
    Создание нового события.
    Это представление позволяет авторизированным пользователям создавать новые события.
    """

    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class EventListView(ListAPIView):
    """
    Получение списка всех событий.
    Это представление позволяет просматривать список всех событий.
    """

    queryset = Event.objects.all()
    serializer_class = EventSerializer


class EventParticipateView(APIView):
    """
    Присоединение к событию.
    Это представление позволяет авторизированным пользователям присоединяться к событиям.
    """

    permission_classes = [IsAuthenticated]

    def post(self, request, event_id):
        event = get_event_by_id(event_id)

        if request.user in event.participants.all():
            return Response({'message': 'Вы уже участник данного события.'})

        event.participants.add(request.user)
        event.save()

        return Response({'message': 'Вы участвуете в событии.'})


class EventCancelParticipationView(APIView):
    """
    Отмена участия в событии.
    Это представление позволяет авторизированным пользователям отменять свое участие в событиях.
    """

    permission_classes = [IsAuthenticated]

    def post(self, request, event_id):
        event = get_event_by_id(event_id)

        if request.user not in event.participants.all():
            return Response({'message': 'Вы не являетесь участником данного события.'})

        event.participants.remove(request.user)
        event.save()

        return Response({'message': 'Вы отменили участие в событии.'})


class EventDeleteView(APIView):
    """
    Удаление события.
    Это представление позволяет удалять события, если пользователь является их создателем.
    """

    permission_classes = [IsAuthenticated]

    def delete(self, request, event_id):
        event = get_event_by_id(event_id)

        if event.creator == request.user:
            event.delete()
            return Response({'message': 'Событие успешно удалено.'})
        else:
            return Response({'message': 'Вы не являетесь создателем события.'}, status=403)
